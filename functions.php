<?php
$text = file_get_contents('text_windows1251.txt');//Считываем полный текст файла в переменную $text
//Выполняем задание (при разбивке по абзацам учтите, что в Windows1251 абзацы разделяются через "\r\n")





/*
 *
 * В папке ksr3 в файле text_windows1251.txt содержится текст и кодировке Windows-1251.
В этой же папке находится скрипт ksr3.php, в котором текст этого файла считывается в переменную $text.
Необходимо выполнить следующие задачи по обработке этого текста с использованием своих пользовательских функций (результат выводится в браузер в кодировке UTF-8):

1. Преобразовать текст в кодировку UTF-8.
2. Определить количество символов, количество слов и количество предложений для каждого абзаца.
3. Вывести первую букву каждого предложения жирным шрифтом.
4. Выделить цветом все слова HTML, PHP, ASP, ASP.NET, Java с любым регистром символов.

Постарайтесь при выполнении задания максимально использовать встроенные PHP-функции для обработки строк и массивов, а не писать собственные аналоги.
Задание непростое, так что обсуждение в нашей группе в вайбере не помешает
 *
 *
 */




//======================================
//1. Преобразовать текст в кодировку UTF-8.
//======================================

$text=mb_convert_encoding($text,'UTF-8','Windows-1251');

//======================================
//2. Определить количество символов, количество слов и количество предложений для каждого абзаца.
//======================================


//получение всех абзацев
$paragraph=explode("\r\n", $text);

//получение всех предложений
$sentence=explode(". ", $text);

function analyseText($arr){
    for($i=0;$i<count($arr);$i++){
        $symbol=mb_strlen($arr[$i]);
        $sentence=count(explode(". ", $arr[$i]));
        $word=count(explode(" ",$arr[$i]));


        echo "В абзаце номер $i: $sentence предложений, $word слов и $symbol символов. <br>";

    }
    return;
}


//echo '<br>';
//echo analyseText($paragraph);
//echo '<br>';



//======================================
//3. Вывести первую букву каждого предложения жирным шрифтом.
//======================================


function makeBoldFirstChar($str){
    return "<span style='font-weight:bold'>".mb_substr($str,0,1)."</span>".mb_substr($str, 1);

}

function makeBoldFirstCharInAll($arr){
    $text=[];
    for($i=0;$i<count($arr);$i++){
        $text[]=makeBoldFirstChar($arr[$i]);
    }
    return implode(". ",$text);
}

//echo '<br>';
//echo makeBoldFirstCharInAll($sentence);
//echo '<br>';




//======================================
//4. Выделить цветом все слова HTML, PHP, ASP, ASP.NET, Java с любым регистром символов.
//======================================

$arr = array("HTML", "PHP", "ASP", "Java");

function emphasizeWord(string $word,string $text){
    return str_replace($word, "<span style =\"color:red;\">$word</span>", $text);

}

function emphasizeText(array $arr,string $text){

    $word = implode("|", $arr);
    $text=emphasizeWord("ASP.NET", $text);
    return preg_replace("~($word)~i", "<span style =\"color:red;\">$1</span>", $text);


}

//echo '<br>';
//echo emphasizeText($arr, $text);
//echo '<br>';





//========================
//Добавление после субботнего занятия
//==================


function main($paragraph)
{
    return $paragraph;
}











////предложения для 1 параграфа,который передается в виде строки

function splitToSentenceOneParagraph(string $paragraph) :array
{
    while($paragraph!= ''){
        $pos = mb_strlen($paragraph);
        $pos1 = mb_strpos($paragraph, '. ');
        if ($pos1 !== false && $pos1 < $pos) $pos = $pos1;
        $pos2 = mb_strpos($paragraph, '! ');
        if ($pos2 !== false && $pos2 < $pos) $pos = $pos2;
        $pos3 = mb_strpos($paragraph, '? ');
        if ($pos3 !== false && $pos3 < $pos) $pos = $pos3;
        $sentences[] = mb_substr($paragraph, 0,$pos + 2);
        $paragraph = mb_substr($paragraph, $pos + 2);


    }

    return $sentences;
}





////абзацы разбивает на предложения
function splitToSentence(array $paragraph) :array
{

    foreach ($paragraph as $p) {
            $pos = mb_strlen($p);
            $pos1 = mb_strpos($p, '. ');
            if ($pos1 !== false && $pos1 < $pos) $pos = $pos1;
            $pos2 = mb_strpos($p, '! ');
            if ($pos2 !== false && $pos2 < $pos) $pos = $pos2;
            $pos3 = mb_strpos($p, '? ');
            if ($pos3 !== false && $pos3 < $pos) $pos = $pos3;
            $sentences[] = mb_substr($p, 0, $pos + 2);
            $p = mb_substr($p, $pos + 2);


        }

        return $sentences;
    }




//echo '<br>';
//var_dump(splitToSentence($paragraph));
//echo '<br>';



//слова с preg_match


function getWordsFromString($string)
{
    if (preg_match_all("/\b(\w+)\b/ui", $string, $matches)) {
        return $matches[1];
    }

    return array();
}


function getWordsFromParagraph(array $paragraph):array
{
    foreach ($paragraph as $p){
        $words[]=getWordsFromString($p);
    }
 return $words;
}
//
//echo '<br>';
//var_dump(getWordsFromString($paragraph[1]));
//echo '<br>';
//
//
//echo '<br>';
//var_dump(getWordsFromParagraph($paragraph));
//echo '<br>';


//без тегов
$sentencesArr=splitToSentence($paragraph);

//echo '<br>';
//var_dump($sentencesArr);
//echo '<br>';



//Кустарный способ убрать теги 3 функциями

function withoutStartTag(array $paragraph) :array
{
    foreach ($paragraph as $p) {
        $length = mb_strlen($p);
        $startTag = mb_strpos($p, '<');
        $endTag = mb_strpos($p, '>');

        if($startTag !== false&&$length>$endTag){
            $sentences[]=mb_substr($p, $endTag+1);



            }
        else $sentences[] = mb_substr($p, 0);
        }



    return $sentences;
}


function withoutEndTag(array $paragraph) :array
{
    foreach ($paragraph as $p) {
        $length = mb_strlen($p);
        $startTag = mb_strpos($p, '<');

        if($startTag !== false){
            $sentences[]=mb_substr($p, 0,$length-($length-$startTag));



        }
        else $sentences[] = mb_substr($p, 0);
    }



    return $sentences;
}


function withoutTags(array $paragraph):array{
    $paragraph=withoutStartTag($paragraph);
    $paragraph=withoutEndTag($paragraph);
    return $paragraph;
}





//
//echo '<br>';
//var_dump(withoutTags($sentencesArr));
//echo '<br>';

///слова и добавление

function isLetter(string $char): bool
{
    //проверка символа $char на символ слова
    return mb_strpos('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyzАБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯабвгдеёжзийклмнопрстуфхцчшщъыьэюя',  $char) !== false;
}

function getWord(string &$sentence): string
{
    //Возвращает слово, удаляя его из
    //$sentence которая передается по ссылке
    $word = '';
    $len = mb_strlen($sentence);
    for ($pos = 0; $pos < $len; $pos++) {
        $char = mb_substr($sentence, $pos, 1);
        if (isLetter($char)) { //проверяем предыдущей функцией на символ слова
            $word .= $char;
        }
        else {
            break;
        }
    }
    if ($pos > 0) {
        $sentence = mb_substr($sentence, $pos);
    }
    return $word;
}


$sent=splitToSentence($paragraph);

//Слова,но не подряд,а хаотично
function getAllWords(array $sentences):array{
    foreach ($sentences as $s) {
        $lengthSentence = mb_strlen($s);

        $words[]=getWord($s);
        $word=mb_strlen(current($words));
        $pos=$lengthSentence-($lengthSentence-$word);
        $s = mb_substr($s, $pos );

    }
    return $words;
}

//Слова попорядку . Виснет браузер.В чем причина?
function getAllWordsInSeries(array $sentences):array{
    foreach ($sentences as $s) {
        for(;$s!=''; $words[]=getWord($s),$word=mb_strlen(current($words)),$pos=$lengthSentence-($lengthSentence-$word),$s = mb_substr($s, $pos ))
        {

            $lengthSentence = mb_strlen($s);
        }


    }


    return $words;
}


//переписывание массива в новый,чтобы не было пустых строк
function unsetFreeSpace($arr)
{
    foreach ($arr as $a) {
        if ($a != "") {
            $arr1[] = $a;
        }
    }
    return $arr1;
}




//
echo '<br>';
//var_dump(unsetFreeSpace(getAllWords($sent)));
//var_dump(getAllWordsInSeries($sent));
echo '<br>';




//формирование ссылок

function linkBuilding($requestedPage,$pagesCount)
{
    for ($i=1; $i<=$pagesCount; $i++)
    {
        if ($i==$requestedPage)
            echo $i.' &nbsp;';
        else
            echo '<a href="' . $_SERVER['SCRIPT_NAME'] . '?page='.$i.'">'.$i.'</a> &nbsp;';
    }

}




